import { question } from "readline-sync";

const dadosCadastrados = []

export function cadastrar() {
    console.log('Cadastrando...');
    let nome = ''
    nome = (question('Nome: '));
   
    let email = ''
    email = (question('Email: '));
    
    let tipoSanguineo = ''
    tipoSanguineo = (question('Tipo sanguineo: '));
    
    let ultimaDoacao = ''
    ultimaDoacao = (question('Ultima data de doação: '));

    const cadastro = {
        name: nome,
        email: email,
        tipoSangue: tipoSanguineo,
        ultimaDataDoacao: ultimaDoacao
    };

    dadosCadastrados.push(cadastro);
}

export function listar() {
    console.log('Listando...');
    console.table(dadosCadastrados);
    
}

export function buscarDoadorPorTipoSanguineo() {
    console.log('Buscando por tipo sanquineo...')
    console.log(question('Qual tipo sanguineo voce quer? '));
    
}

export function buscarDoadorPorDataUltimaDoacao() {
    console.log('Buscando por data da ultima doação...')
}